# gitlab pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/gitlab?branch=main)](https://gitlab.com/buildgarden/pipelines/gitlab/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/gitlab)](https://gitlab.com/buildgarden/pipelines/gitlab/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

GitLab-specific machinery for CI.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Usage

### Create a release

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/gitlab
    file:
      - gitlab-release.yml
```
